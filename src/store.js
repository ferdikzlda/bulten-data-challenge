import React, { createContext, useReducer } from "react";
import { node } from "prop-types";
import couponReducer from "./pages/HomePage/reducer";

const coupon = [];
export const MainContext = createContext(coupon);

const Store = ({ children })=> {
  const [state, dispatch] = useReducer(couponReducer, coupon);
  const value = { state, dispatch };
  return <MainContext.Provider value={value}>{children}</MainContext.Provider>;
}

Store.defaultProps = {
  children: null
};
Store.propTypes = {
  children: node
};

export default Store;
