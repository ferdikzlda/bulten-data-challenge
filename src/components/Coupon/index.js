import React from 'react';
import style from './style.module.scss';

const Coupon = ({ state }) => {
    let totalRate = 1;
    state.forEach(item => {
        totalRate *= item.Rate;
    });
    return (
        <div className={style.container}>
            <ul className={style.ul}>
                {state.map((item, key) => (
                    <li key={key}>
                        <span>
                            Kod:{item.C} Maç:{item.N} Oran:{item.Rate}
                        </span>
                        <hr />
                    </li>
                ))}
            </ul>
            <span className={style.totalRate}>
                TOPLAM ORAN: {state.length > 0 ? totalRate.toFixed(3) : 0}
            </span>
        </div>
    )
}
export default Coupon;