const couponReducer = (state, action) => {
  switch (action.type) {
    case "addBet":
      state.push(action.value);
      return [...state];
    case "removeBet":
      const index = state.findIndex(obj => obj.C === action.value.C);
      return [...[...state.slice(0, index), ...state.slice(index + 1)]];
    case "updateBet":
      state[state.findIndex(obj => obj.C === action.value.C)] = action.value;
      return [...state];
    default:
      return state;
  }
}
export default couponReducer;