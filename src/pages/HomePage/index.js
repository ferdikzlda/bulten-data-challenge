import React, { useContext } from "react";
import { Column, Table, WindowScroller, AutoSizer } from "react-virtualized";
import { Events } from "./bulten_data.json";
import { MainContext } from "../../store";
import { addBet, removeBet, updateBet } from "./action";
import Coupon from '../../components/Coupon';
import style from './style.module.scss';

const EventList = Object.values(Events);

const CellClickFunc = (dispatch, bet, cellData, dataKey, rowData) => () => {
  if(dataKey === 'Comment' ||dataKey === 'MSB') return false;
  if (!bet) {
    dispatch(
      addBet({
        C: rowData.C,
        N: rowData.N,
        Rate: cellData,
        Selection: dataKey
      })
    );
  } else if (bet && bet.Selection !== dataKey) {
    dispatch(
      updateBet({
        C: rowData.C,
        N: rowData.N,
        Rate: cellData,
        Selection: dataKey
      })
    );
  } else if (bet && bet.Selection === dataKey) {
    dispatch(
      removeBet({
        C: rowData.C
      })
    );
  }
};

const CellRenderer = (state, dispatch) => ({ cellData, dataKey, rowData }) => {
  let bet = state[state.findIndex(obj => obj.C === rowData.C)];
  return (
    <div
      style={
        bet && bet.Selection === dataKey && dataKey !== 'Comment' && dataKey !== 'MSB'
          ? { backgroundColor: "yellow", cursor: "pointer" }
          : { cursor: "pointer" }
      }
      onClick={CellClickFunc(dispatch, bet, cellData, dataKey, rowData)}
    >
      {cellData}
    </div>
  );
};

const MatchNameCellRenderer = ({cellData}) => {
  return <div className={style.matchNameCell}>{cellData}</div>;
};

const HomePage = () => {
  const cellWidth = 40;
  const { state, dispatch } = useContext(MainContext);

  return (
    <>
      <WindowScroller>
        {({ height, isScrolling, onChildScroll, scrollTop }) => (
          <AutoSizer>
            {({ width }) => (
              <Table
                width={width}
                autoHeight
                height={height}
                isScrolling={isScrolling}
                onScroll={onChildScroll}
                scrollTop={scrollTop}
                headerHeight={50}
                rowHeight={50}
                rowCount={EventList.length}
                rowGetter={({ index }) => EventList[index]}
              >
                <Column
                  label="Event Count:@@@@@@"
                  dataKey="C"
                  width={300}
                  cellRenderer={MatchNameCellRenderer}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return (
                      rowData["C"] + " " + rowData["T"] + " " + rowData["N"]
                    );
                  }}
                />
                <Column
                  label="Yorumlar"
                  dataKey="Comment"
                  width={100}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "Yorumlar";
                  }}
                />
                <Column
                  label=""
                  dataKey="MSB"
                  width={20}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "4";
                  }}
                />
                <Column
                  label="1"
                  dataKey="1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["1"].OC["0"].O;
                  }}
                />
                <Column
                  label="X"
                  dataKey="X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["1"].OC["1"].O;
                  }}
                />
                <Column
                  label="2"
                  dataKey="2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="Alt"
                  dataKey="Alt"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["5"].OC["25"]["O"];
                  }}
                />
                <Column
                  label="Üst"
                  dataKey="Üst"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["5"].OC["26"]["O"];
                  }}
                />
                <Column
                  label="H1"
                  dataKey="H1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="1"
                  dataKey="1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="X"
                  dataKey="X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="2"
                  dataKey="2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="H2"
                  dataKey="H2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="1-X"
                  dataKey="1-X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["3"]["O"];
                  }}
                />
                <Column
                  label="1-2"
                  dataKey="1-2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["4"]["O"];
                  }}
                />
                <Column
                  label="X-2"
                  dataKey="X-2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["5"]["O"];
                  }}
                />
                <Column
                  label="Var"
                  dataKey="Var"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="Yok"
                  dataKey="Yok"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
              </Table>
            )}
          </AutoSizer>
        )}
      </WindowScroller>
      <Coupon state={state} />
    </>
  );
};

export default HomePage;
