export const addBet = value => ({ type: "addBet", value });
export const removeBet = value => ({ type: "removeBet", value });
export const updateBet = value => ({ type: "updateBet", value });
